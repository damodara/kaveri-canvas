import { Point, Path, Color, Event, HitResult, ToolEvent } from 'paper';
// eslint-disable-next-line import/no-cycle
import { Overlay } from '../overlay';

export interface ITool {
    name: string;
    logoClass: string;
    idPrefix: string;
    toolTip: string;

    init(): void;
    createShape(initialPoint: Point, overlay: Overlay): Path;
    updateSelection(selected: boolean, item: Path, overlay: Overlay): void;
    onResize(item: Path, overlay: Overlay): void;
    onHover(activate: boolean, shape: Path, hoverWidth: number, hoverColor: Color): void;
    onMouseUp(event: ToolEvent, overlay: Overlay): void;
    translate(event: Event, overlay: Overlay): void;
    rotate(event: Event, overlay: Overlay): void;
    onMouseDrag(event: ToolEvent, overlay: Overlay): void;
    onMouseMove(event: ToolEvent, overlay: Overlay): void;
    setCursor(hitResult: HitResult, overlay: Overlay): void;
    onMouseDown(event: ToolEvent, overlay: Overlay): void;
    onDoubleClick(event: Event, overlay: Overlay): void;
}
