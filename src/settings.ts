export interface IDrawingToolSettings {
    'doubleClickReactionTime': number,
    'strokeColor': string,
    'fillColor': string,
    'fillColorAlpha': number,
    'shapeHandleSize': number,
    'fixedShapeSize': number,
    'newlyCreatedShapeStrokeWidthFactor': number,
    'hoverColor': string,
}

export interface ISettings {
    drawingToolSettings: IDrawingToolSettings,
}

export const defaultDrawingToolSettings: IDrawingToolSettings = {
    doubleClickReactionTime: 300,
    strokeColor: 'deepSkyBlue',
    fillColor: 'deepSkyBlue',
    fillColorAlpha: 0.0,
    shapeHandleSize: 10,
    fixedShapeSize: 10,
    newlyCreatedShapeStrokeWidthFactor: 5,
    hoverColor: 'yellow',
};

export const defaultSettings: ISettings = {
    drawingToolSettings: defaultDrawingToolSettings,
};
