// @ts-ignore
import osd from 'openseadragon';
import Emitter from 'component-emitter';
import deepmerge from 'deepmerge';
import paper, { Path, Segment, PaperScope, ToolEvent } from 'paper';

import { IDrawingToolSettings, defaultDrawingToolSettings } from './settings';
// eslint-disable-next-line import/no-cycle
import { ITool } from './tools/ITool';
import { IIIFCanvas } from './iiif/interfaces';


export interface IOverlayState {
    canvas: IIIFCanvas,
    drawingToolSettings: IDrawingToolSettings,
}


export class MouseToolMixin {
    public overlay!: Overlay;

    public static create(paperScope: PaperScope) {
        const tool = new paperScope.Tool();
        Object.assign(tool, MouseToolMixin.prototype);
        return tool;
    }

    public onMouseUp(event: ToolEvent) {
        if (this.overlay.disabled) {
            return;
        }
        event.stopPropagation();
        if (this.overlay.currentTool) {
            this.overlay.currentTool.onMouseUp(event, this.overlay);
        }
    }

    public onMouseDrag(event: ToolEvent) {
        if (this.overlay.disabled) {
            // @ts-ignore
            const nativeEvent = event.event; // NOTE
            const absolutePoint = {
                x: nativeEvent.clientX,
                y: nativeEvent.clientY,
            };
            this.overlay.eventEmitter.emit(`updateTooltips.${this.overlay.id}`, [event.point, absolutePoint]);
        } else {
            event.stopPropagation();
            if (this.overlay.currentTool) {
                if (this.overlay.currentTool.name === 'Freehand' && this.overlay.mode === 'create') {
                    // freehand create needs to use mouse position because bounds are not accurate until shape is finished
                    event.point = this.overlay.getMousePositionInImage(event.point);
                    // @ts-ignore
                    event.delta = event.point - event.lastPoint; // NOTE
                } else {
                    // eslint-disable-next-line no-lonely-if
                    if (this.overlay.path) {
                        const { bounds } = this.overlay.path;
                        // we already have a shape, and we are moving it, need to account for that, rather than mouse position
                        event.delta = this.overlay.adjustDeltaForShape(event.lastPoint, event.point, event.delta, bounds);
                    }
                }
                // we may not currently have a tool if the user is in edit mode and didn't click on an editable shape
                this.overlay.currentTool.onMouseDrag(event, this.overlay);
            }
        }
        // @ts-ignore
        this.overlay.paperScope.view.draw();
    }

    public onMouseMove(event: ToolEvent) {
        if (this.overlay.disabled) {
            // @ts-ignore
            const nativeEvent = event.event;
            const absolutePoint = {
                x: nativeEvent.clientX,
                y: nativeEvent.clientY,
            };
            this.overlay.eventEmitter.emit(`updateTooltips.${this.overlay.id}`, [event.point, absolutePoint]);
        } else {
            // We are in drawing mode
            if (this.overlay.paperScope.project!.hitTest(event.point!, this.overlay.hitOptions)) {
                this.overlay.eventEmitter.emit(`POINTER_CURSOR.${this.overlay.id}`);
            } else if (this.overlay.currentTool && !this.overlay.path) {
                this.overlay.eventEmitter.emit(`CROSSHAIR_CURSOR.${this.overlay.id}`);
            } else {
                this.overlay.eventEmitter.emit(`DEFAULT_CURSOR.${this.overlay.id}`);
            }
            event.stopPropagation();
            if (this.overlay.currentTool) {
                this.overlay.currentTool.onMouseMove(event, this.overlay);
            }
        }
        // @ts-ignore
        this.overlay.paperScope.view.draw();
    }

    public onMouseDown(event: ToolEvent) {
        if (this.overlay.disabled) {
            return;
        }
        event.stopPropagation();
        const date = new Date();
        const time = date.getTime();
        let hitResult;

        if (time - this.overlay.latestMouseDownTime < this.overlay.dts.doubleClickReactionTime) {
            this.overlay.latestMouseDownTime = time;
            this.onDoubleClick(event);
        } else {
            this.overlay.latestMouseDownTime = time;
            hitResult = this.overlay.paperScope.project!.hitTest(event.point!, this.overlay.hitOptions);

            if (this.overlay.mode !== 'create' && this.overlay.mode !== '') {
                this.overlay.mode = '';
                this.overlay.currentTool = null;
            }

            if (hitResult && this.overlay.mode !== 'create') {
                let overlayEditable = false;
                if (typeof hitResult.item!.data.editable !== 'undefined') {
                    overlayEditable = hitResult.item!.data.editable; // TODO should remove this editable variable when persisting the svg (will reduce length of string/memory)
                }
                // if item is part of shape it is editable
                // part of shape items only appear when the shape is selected
                // @ts-ignore
                // eslint-disable-next-line
                if (hitResult.item!._name.toString().indexOf(this.overlay.partOfPrefix) !== -1) {
                    overlayEditable = true;
                }
                if (!overlayEditable) {
                    return;
                }
            }

            // @ts-ignore
            if (hitResult && (!this.overlay.currentTool || (hitResult.item!._name.toString().indexOf(this.overlay.currentTool.idPrefix) === -1 && this.overlay.mode === ''))) {
                // @ts-ignore
                let prefix = hitResult.item!._name.toString();
                prefix = prefix.substring(0, prefix.indexOf('_') + 1);

                // nasty workaround some names contain `_` inside their name
                // @ts-ignore
                let longPrefix = hitResult.item!._name.toString().split('_');

                longPrefix = `${longPrefix[0]}_${longPrefix[1]}_`;

                for (let j = 0; j < this.overlay.tools.length; j += 1) {
                    if (this.overlay.tools[j].idPrefix === prefix || this.overlay.tools[j].idPrefix === longPrefix) {
                        this.overlay.eventEmitter.emit(`toggleDrawingTool.${this.overlay.id}`, this.overlay.tools[j].logoClass);
                        break;
                    }
                }
            }

            if (this.overlay.currentTool) {
                this.overlay.eventEmitter.emit(`HUD_REMOVE_CLASS.${this.overlay.id}`, ['.hud-dropdown', 'hud-disabled']);
                event.point = this.overlay.getMousePositionInImage(event.point);
                this.overlay.currentTool.onMouseDown(event, this.overlay);
            }
        }
        this.overlay.hover();
        this.overlay.paperScope.view.draw();
    }

    public onDoubleClick(event: ToolEvent) {
        event.stopPropagation();
        if (this.overlay.currentTool) {
            this.overlay.currentTool.onDoubleClick(event, this.overlay);
        }
    }
}


export class Overlay {
    public id: string;

    public viewer: osd.Viewer;

    public state: IOverlayState;

    public iiifCanvas: IIIFCanvas;

    public dts: IDrawingToolSettings;

    public eventEmitter: Emitter;

    public disabled = true;

    public inEditOrCreateMode = false;

    public mode: '' | 'create' | 'translate' | 'deform' | 'rotate' | 'edit' = '';

    public draftPaths: Path[] = [];

    public editedPaths: Path[] = [];

    public hoveredPath: Path | null = null;

    public path?: Path | null = null;

    public segment: Segment | null = null;

    public latestMouseDownTime = -1;

    public availableAnnotationDrawingTools: ITool[] = [];

    public dashArray = [];

    public strokeWidth = 1;

    public selectedColor = '#004c66';

    public hitOptions = {
        handles: true,
        stroke: true,
        segments: true,
        tolerance: 5,
    };

    public tools: ITool[] = [];

    public currentTool: ITool | null = null;

    public canvas: HTMLCanvasElement;

    public eventsSubscriptions = [];

    public lastAngle!: number;

    public paperScope!: paper.PaperScope;

    public cursorLocation: any;

    public mouseToolKey!: string;

    public mouseTool!: paper.Tool;

    public horizontallyFlipped = false;


    constructor({ id, viewer, eventEmitter, state }: {id: string, viewer: any, eventEmitter: Emitter, state: IOverlayState}) {
        this.id = id;
        this.viewer = viewer;

        this.state = state;
        this.iiifCanvas = this.state.canvas;
        this.dts = deepmerge(this.state.drawingToolSettings, defaultDrawingToolSettings, {
            arrayMerge: (a1: any[], a2: any[]) => a2,
        });

        this.eventEmitter = eventEmitter;

        this.canvas = document.createElement('canvas');
        this.canvas.setAttribute('tabindex', '0');
        this.canvas.addEventListener('mousedown', () => {
            this.canvas.focus();
        });
        this.canvas.id = `draw_canvas_${this.id}`;
        this.viewer.canvas.appendChild(this.canvas); // TODO position to be setted to absolute.

        this.resize();
        this.show();
        this.init();
    }

    public init() {
        this.lastAngle = 0;
        this.paperScope = new paper.PaperScope();
        this.paperScope.setup(this.canvas);
        this.paperScope.activate();
        // @ts-ignore
        this.paperScope.project.options.handleSize = this.shapeHandleSize;

        this.paperScope.view.onFrame = () => {
            // @ts-ignore
            if (this.paperScope.snapPoint) {
                // @ts-ignore
                this.paperScope.snapPoint.remove();
                // @ts-ignore
                this.paperScope.snapPoint = null;
            }
            if (this.path && !this.path.closed && this.cursorLocation && this.currentTool && this.currentTool.idPrefix.includes('rough_path_')) {
                // @ts-ignore
                const distanceToFirst = this.path.segments[0].point.getDistance(this.cursorLocation);
                // @ts-ignore
                if (this.path.segments.length > 1 && distanceToFirst < this.hitOptions.tolerance) {
                    // @ts-ignore
                    this.paperScope.snapPoint = new this.paperScope.Path.Circle({
                        name: 'snapPoint',
                        // @ts-ignore
                        center: this.path.segments[0].point,
                        // @ts-ignore
                        radius: this.hitOptions.tolerance / this.paperScope.view.zoom,
                        fillColor: this.path.strokeColor,
                        strokeColor: this.path.strokeColor,
                        strokeWidth: this.path.strokeWidth,
                    });
                }
            }
        };

        // Key for saving mouse tool as data attribute
        // TODO: It seems its main use is for destroy the old paperjs mouse tool
        // when a new Overlay is instantiated. Maybe a better scheme can be
        // devised in the future?
        this.mouseToolKey = `draw_canvas_${this.id}`;

        this.setMouseTool();
        this.listenForActions();
    }

    public setMouseTool() {
        this.removeMouseTool();
        this.paperScope.activate();

        this.mouseTool = new this.paperScope.Tool();
        // @ts-ignore
        this.mouseTool.overlay = this;
        this.mouseTool.onMouseUp = this.onMouseUp;
        this.mouseTool.onMouseDrag = this.onMouseDrag;
        this.mouseTool.onMouseMove = this.onMouseMove;
        this.mouseTool.onMouseDown = this.onMouseDown;
        // @ts-ignore
        this.mouseTool.onDoubleClick = this.onDoubleClick;
        this.mouseTool.onKeyDown = () => {};
    }

    removeMouseTool() {
        if (this.mouseTool) {
            this.mouseTool.remove();
        }
    }

    listenForActions() {
        return this;
    }

    deleteShape(shape: any) {
        for (let i = 0; i < this.draftPaths.length; i += 1) {
            // @ts-ignore
            // eslint-disable-next-line no-underscore-dangle
            if (this.draftPaths[i]._name.toString() === shape._name.toString()) {
                this.draftPaths.splice(i, 1);
                break;
            }
        }
        this.removeFocus();
        shape.remove();
    }

    getMousePositionInImage(mousePosition: any) {
        const newMousePosition = mousePosition;

        if (newMousePosition.x < 0) {
            newMousePosition.x = 0;
        }
        if (newMousePosition.x > this.canvas.width) {
            newMousePosition.x = this.canvas.width;
        }
        if (newMousePosition.y < 0) {
            newMousePosition.y = 0;
        }
        if (newMousePosition.y > this.canvas.height) {
            newMousePosition.y = this.canvas.height;
        }
        if (this.horizontallyFlipped) {
            newMousePosition.x = this.canvas.width - newMousePosition.x;
        }
        return newMousePosition;
    }

    adjustDeltaForShape(lastPoint: any, currentPoint: any, delta: any, bounds: any) {
        // first check along x axis
        if (lastPoint.x < currentPoint.x) {
            // moving to the right, delta should be based on the right most edge
            if (bounds.x + bounds.width > this.canvas.width) {
                delta.x = this.canvas.width - (bounds.x + bounds.width);
            }
        } else {
            // moving to the left, prevent it from going past the left edge.  if it does, use the shapes x value as the delta
            // eslint-disable-next-line no-lonely-if
            if (bounds.x < 0) {
                delta.x = Math.abs(bounds.x);
            }
        }
        if (this.horizontallyFlipped) {
            delta.x = -delta.x;
        }
        // check along y axis
        if (lastPoint.y < currentPoint.y) {
            // moving to the bottom
            if (bounds.y + bounds.height > this.canvas.height) {
                delta.y = this.canvas.height - (bounds.y + bounds.height);
            }
        } else {
            // moving to the top
            // eslint-disable-next-line no-lonely-if
            if (bounds.y < 0) {
                delta.y = Math.abs(bounds.y);
            }
        }
        return delta;
    }
}
