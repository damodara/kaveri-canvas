import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
//  import typescript from 'rollup-plugin-typescript2';
import sourceMaps from 'rollup-plugin-sourcemaps';
import json from 'rollup-plugin-json';
// import { terser } from 'rollup-plugin-terser';
// @ts-ignore
import pkg from './package.json';


export default [
    // browser friendly UMD and IIFE builds
    {
        input: 'lib/index.js',
        output: {
            name: 'kanvas',
            file: pkg.browser,
            format: 'umd',
            globals: {
                // 'vedavaapi-web': 'vweb',
            },
        },
        plugins: [
            resolve(),
            commonjs(),
            /* typescript({
                // eslint-disable-next-line global-require
                typescript: require('typescript'),
            }), */
            // terser(),
        ],
        external: [
            // 'vedavaapi-web',
        ],
    },

    {
        input: 'lib/index.js',
        output: [
            { file: pkg.main, name: 'kanvas', format: 'umd', sourcemap: true },
            { file: pkg.module, format: 'es', sourcemap: true },
        ],
        // Indicate here external modules you don't wanna include in your bundle (i.e.: 'lodash')
        external: [
            // 'vedavaapi-web',
        ],
        watch: {
            include: 'src/**',
        },
        plugins: [
            // Allow json resolution
            json(),
            // Compile TypeScript files
            // typescript({ useTsconfigDeclarationDir: true }),
            // Allow bundling cjs modules (unlike webpack, rollup doesn't understand cjs)
            commonjs(),
            // Allow node_modules resolution, so you can use 'external' to control
            // which external modules to include in the bundle
            // https://github.com/rollup/rollup-plugin-node-resolve#usage
            resolve(),

            // Resolve source maps to the original source
            sourceMaps(),
        ],
    },
];
